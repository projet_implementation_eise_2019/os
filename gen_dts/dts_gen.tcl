open_hw_design system.hdf
set_repo_path device-tree-xlnx
create_sw_design impl -proc ps7_cortexa9_0 -os device_tree
set_property CONFIG.periph_type_overrides "{BOARD zedboard}" [get_os]
generate_bsp -dir dt/
exit
