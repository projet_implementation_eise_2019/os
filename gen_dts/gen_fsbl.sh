#!/bin/bash
    
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

mkdir -p $DIR/fsbl
rm -rf $DIR/fsbl/*
cd $DIR

hsi -source $DIR/gen_fsbl.tcl
cp fsbl/executable.elf $DIR/../impl/board/zynq-zedboard/fsbl.elf
rm -f hsi* ps7*
