#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

mkdir -p $DIR/dt
rm -rf $DIR/dt/*
rm -f $DIR/../impl/zedboard/zynq-zedboard/dts/*

cd $DIR
git clone https://github.com/Xilinx/device-tree-xlnx
cd device-tree-xlnx && git checkout xilinx-v2019.1
cd ..

hsi -source $DIR/dts_gen.tcl

find $DIR/dt/* -name "*.h" -exec cp {} $DIR/../impl/board/zynq-zedboard/dts/ \;
cp $DIR/dt/system-top.dts $DIR/../impl/board/zynq-zedboard/dts/system.dts
find $DIR/dt/* -name "*.dtsi" -exec cp {} $DIR/../impl/board/zynq-zedboard/dts/ \;
cp *.bit $DIR/../impl/board/zynq-zedboard/fpga.bit
rm -rf device-tree-xilinx
rm -f hsi* ps7*
