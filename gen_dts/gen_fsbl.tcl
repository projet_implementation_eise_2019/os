open_hw_design system.hdf
create_sw_design impl_fsbl -proc ps7_cortexa9_0 -os standalone -app zynq_fsbl
generate_app -os standalone -proc ps7_cortexa9_0 -app zynq_fsbl -compile -sw fsbl -dir fsbl/
exit
