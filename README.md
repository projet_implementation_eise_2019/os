# OS

__Intructions générales pour le build__
----

1) Cloner ce dépot et se dépacer à sa racine:
``` bash
git clone https://gitlab.com/projet_implementation_eise_2019/os.git
cd OS
```

----
2) Cloner buildroot
``` bash
git clone https://github.com/buildroot/buildroot.git
```

----
3) Configuration initiale
``` bash
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot zynq_zedboard_defconfig
```

----
4) Configuration de linux
``` bash
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot linux-menuconfig
```
Allez dans "CPU Power Management" puis "CPU Frequency scaling" puis "Default CPUFreq govenor". Choisissez "userspace".

----
5) Build de la distrib
``` bash
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot
```

__Reconstruire une cible après mise à jour__
----

Supprimer le dossier de build, reconstruire la cible et mettre à jour l'image ainsi que le rootfs:
``` bash
rm -rf output/build/<nom du packet>-<version>
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot <nom-du-packet>-rebuild
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot
```

---
Example avec le noyau linux:
``` bash
rm -rf output/build/linux-xilinx-v2018.2
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot linux-rebuild
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot
```

__Mettre à jour le device tree, le FSBL et le bitstream__
----

1) S'assurer de bien avoir les outils Xilinx dans le PATH
``` bash
source <vivado install dir>/settings64.sh
```

----
2) Exporter le hardware avec vivado (une fois que le bitstream a été généré)
Cliquez sur "Fichier" -> "export hardware".
Cochez la case "include bitstream".
Puis copiez le fichier ".hdf" dans le dossier "gen\_dts"
``` bash
cp <vivado project directory>/<vivado project name>.sdk/system.hdf gen_dts/
```
----
3) Générer le device tree et le FSBL et mettre à jour le dossier impl
``` bash
bash gen_dts/dts_update.sh
bash gen_dts/gen_fsbl.sh
```

----
4) Regénérer linux + l'image de boot
``` bash
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot linux-rebuild
make BR2_EXTERNAL=$PWD/impl O=$PWD/output -C buildroot
```

__Arborescence__
----

L'arborescence produite par buildroot est la suivante:
```
+ output
| + build (contient l'emsemble des packages buildroot compilés).
| + images (contient les fichiers à copier sur la carte SD)
| | + boot (contient les fichiers à copier sur la partition de boot de la carte SD)
| + host (contient les outils pour le host: cross-compilateurs, bibliothèques compilées, debuggers, ...)
| + target (contient le rootfs en construction pour la cible, à ne pas utiliser: ce rootfs n'est pas toujours complet)
| + staging (symlink vers les cross-compilateurs contenu dans le dossier host)
```

__Création de la carte SD bootable une fois le build fini__
----

1) Identifier la carte SD et la demonter
``` bash
fdisk -l
umount /dev/mmcblk<N>
```
Remplacez N par le numéro que vous aurez identifié avec fdisk.

----
2) Effacer la carte SD (écrire des zeros au début pour effacer la table des partitions)

``` bash
dd if=/dev/zero of=/dev/mmcblk<N> bs=1M count=50
```

----
3) Créer 2 partitions (une pour le boot de ~100M, l'une pour le rootfs avec le reste de l'espace disponible) 

Vous pouvez utiliser fdisk ou gparted par example.

----
4) Formater les partitions (boot en fat32, rootfs en ext4)

``` bash
mkfs.vfat -F 32 -n boot /dev/mmcblk<N>p1
mkfs.ext4 -L rootfs /dev/mmcblk<N>p2
```

----
5) Copier les fichiers générés sur la carte SD et démonter les volumes

``` bash
cp output/images/boot/* /media/<user>/boot/
tar xf output/images/rootfs.tar -C /media/<user>/rootfs
umount /media/<user>/* 
```
